<div class="news-letter">
    <div class="container">
        <div class="join">
            <h6>Поиск по магазину</h6>
            <div class="sub-left-right">
                <form>
                    <input type="text" name="s" value="Введите поисковый запрос" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Введите поисковый запрос';}" />
                    <input type="submit" value="ПОИСК" />
                </form>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="footer_top">
            <div class="span_of_4">
                <?php dynamic_sidebar('footer') ?>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="cards text-center">
            <img src="<?php bloginfo('template_directory'); ?>/images/cards.jpg" alt="" />
        </div>
        <div class="copyright text-center">
            <p>© 2015 Eshop. All Rights Reserved | Design by   <a href="http://w3layouts.com">  W3layouts</a></p>
        </div>
    </div>
</div>
<?php wp_footer(); ?>
<script>
    jQuery(function($) {
        $('#pagination-posts').on('change', function() {
           var  countPage = $('#pagination-posts').val();

           var args = {};

           var query = window.location.search.substring(1);
           var parts = query.split('&');

           for(var i = 0; i < parts.length; ++i) {
               var pos = parts[i].indexOf('=');
               if(pos == -1) {
                  continue;
               }
               var name = parts[i].substring(0, pos);
               var value = parts[i].substring(pos+1);

               args[name] = value;
           }
            args['ppp'] = countPage;
           window.location.href = '?'+$.param(args);
        });
    });
</script>
</body>
</html>