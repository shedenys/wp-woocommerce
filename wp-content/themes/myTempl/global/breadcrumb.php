<?php
/**
 * Shop breadcrumb
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/breadcrumb.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 * @see         woocommerce_breadcrumb()
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! empty( $breadcrumb ) ) {

	echo $wrap_before;

	foreach ( $breadcrumb as $key => $crumb ) {

		echo $before;

		if ( ! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1 ) {
			echo '<a href="' . esc_url( $crumb[1] ) . '">' . esc_html( $crumb[0] ) . '</a>';
		} else {
			echo esc_html( $crumb[0] );
		}

		echo $after;

		if ( sizeof( $breadcrumb ) !== $key + 1 ) {
			echo $delimiter;
		}
	}

	echo $wrap_after;

}

// Taxonomy характеризует категорию
$taxonomy = 'product_cat';
// Поиск необходимых элементов taxonomy (в этом сучае - категорий)
$terms = get_terms(array(
	'taxonomy' => $taxonomy,
	'hide_empty' => false
));
// идентификаторы категорий
$ids = array();
foreach($terms as $term) {
	$ids[] = $term->term_id;
}
// Получаем текущую категорию
$obj = get_queried_object();
$termC = $obj->term_id;
// Ищем позицию категории в массивах идентификаторов
$positionC = array_search($termC, $ids);
// Индекс следующей категории
$prev_term = $positionC + 1;

if($prev_term >= 0) {
	// Получаем данные по категории
	$prevTerm = get_term($ids[$prev_term], $taxonomy);
	if($prevTerm && !is_cart() && !is_checkout() && !is_account_page() && !is_page('wishlist')) {
		echo '<p class="back"><a href="'.get_term_link($prevTerm->term_id, $taxonomy).'">'.$prevTerm->name.'</a></p>';
	}
}
?>
	<div class="clearfix"></div>
</div>