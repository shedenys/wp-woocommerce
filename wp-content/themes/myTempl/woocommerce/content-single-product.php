<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?>

<?php
do_action('woocommerce_before_single_product');

if ( post_password_required() ) {
    echo get_the_password_form();
    return;
}
?>

<div class="col-md-5 zoom-grid">

    <?php do_action('woocommerce_before_single_product_summary'); ?>
</div>

<div class="col-md-7 dress-info">

    <?php do_action('woocommerce_single_product_summary');?>

    <?php
    wp_enqueue_script('flexslider', get_template_directory_uri().'/js/jquery.flexslider.js');
    wp_enqueue_script('imagezoom', get_template_directory_uri().'/js/imagezoom.js');
    ?>
    <script>
        // Can also be used with $(document).ready()
        jQuery(window).load(function() {
            jQuery('.flexslider').flexslider({
                animation: "slide",
                controlNav: "thumbnails"
            });
        });
    </script>
</div>
<div class="clearfix"></div>
<div class="reviews-tabs">
    <!-- Main component for a primary marketing message or call to action -->
    <?php do_action('woocommerce_after_single_product_summary');?>

    <script type="text/javascript">
        jQuery( '#myTab a' ).click( function ( e ) {
            e.preventDefault();
            jQuery( this ).tab( 'show' );
        } );

        jQuery( '#moreTabs a' ).click( function ( e ) {
            e.preventDefault();
            $( this ).tab( 'show' );
        } );

        ( function( ) {
            // Test for making sure event are maintained
            jQuery( '.js-alert-test' ).click( function () {
                alert( 'Button Clicked: Event was maintained' );
            } );
            fakewaffle.responsiveTabs( [ 'xs', 'sm' ] );
        } )( jQuery );

    </script>
</div>