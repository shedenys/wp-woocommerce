<?php
/**
 * Loop Add to Cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/add-to-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

// Содержимое текущей итерации цикла
global $product;

// Есть ли цена у текущего товара
if($price_html = $product->get_price_html()) {

	if(is_shop() || is_product()) {
		echo apply_filters(
		// фильтр на событие формирования кнопки добавления в корзину
			'woocommerce_loop_add_to_cart_link',
			sprintf('<p><a href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="item_add %s"><i></i> <span class="item_price">%s</span></a></p>',
				// путь ссылки на страницу корзины
				esc_url($product->add_to_cart_url()),
				// определение количества. esc_attr - преобразует спец.символы в html-сущности
				esc_attr(isset($quantity) ? $quantity: 1),
				esc_attr($product->get_id()),
				esc_attr($product->get_sku()),
				// значения доп. классов, которые будут добавлены к текущей ссылке
				esc_attr(isset($class) ? $class: 'button'),
				// цена
				$price_html
			)
		);
	}
	else {

        echo apply_filters( 'woocommerce_loop_add_to_cart_link',
        sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
            esc_url( $product->add_to_cart_url() ),
            esc_attr( isset( $quantity ) ? $quantity : 1 ),
            esc_attr( $product->get_id() ),
            esc_attr( $product->get_sku() ),
            esc_attr( isset( $class ) ? $class : 'button' ),
            esc_html( $product->add_to_cart_text() )
        ),
        $product );

	}

}