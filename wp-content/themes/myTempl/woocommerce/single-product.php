<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Шапка
get_header();
// Навигационная панель
get_navigation();

?>

<!-- content-section-starts -->
<div class="container">

    <?php do_action('woocommerce_before_main_content'); ?>
    <div class="products-page">

        <?php get_sidebar(); ?>

        <div class="new-product">

            <?php while(have_posts()): the_post(); ?>
                <?php wc_get_template_part('content', 'single-product'); ?>
            <?php endwhile; ?>

        </div>
        <div class="clearfix"></div>
    </div>
</div>

<?php
do_action('woocommerce_after_main_content');
?>
<!-- content-section-ends -->

<?php
get_sidebar('content-bottom');
get_footer();
?>