<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Убираем Breadcrumbs с главной страницы
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

get_header(); ?>
<?php get_navigation(); ?>
<?php get_template_part('home', 'banner'); ?>
<!-- content-section-starts-here -->
<?php do_action('woocommerce_before_main_content'); ?>
    <div class="online-strip">
        <?php if(function_exists('dynamic_sidebar')): ?>
        <div class="col-md-4 follow-us">
            <?php dynamic_sidebar('follow_us'); ?>
        </div>
        <?php endif; ?>
        <?php if(function_exists('dynamic_sidebar')): ?>
        <div class="col-md-4 shipping-grid">
            <?php dynamic_sidebar('shipping'); ?>
        </div>
        <?php endif; ?>
        <div class="col-md-4 online-order">
            <p>Order online</p>
            <h3>Tel:<?php echo get_option('my_phone'); ?></h3>
        </div>
        <div class="clearfix"></div>
    </div>

    <?php
    // Хук вывода текста описания страницы. Если не используется - можно убрать
    do_action('woocommerce_archive_description');
    ?>
    <?php
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => 9,
        // "рекомендуемый" товар
//            'meta_key' => '_featured',
//            'meta_value' => 'yes'
    );
    global $wp_query;
    $wp_query = new WP_Query($args);
    if($wp_query->have_posts()) :
    ?>
        <?php
        // Вызов начала цикла
        woocommerce_product_loop_start(); ?>
            <?php
            while($wp_query->have_posts()):
                $wp_query->the_post();
                wc_get_template_part('content', 'product');
                ?>
                <?php
            endwhile; ?>
            <div class="clearfix"></div>
        <?php
        // Вызов конца цикла
        woocommerce_product_loop_end(); ?>
    <?php
    endif;
    ?>
<?php do_action('woocommerce_after_main_content'); ?>
<?php get_sidebar('content-bottom'); ?>
<!-- content-section-ends-here -->
<?php get_footer(); ?>