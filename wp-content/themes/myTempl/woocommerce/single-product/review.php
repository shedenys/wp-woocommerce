<?php
/**
 * Review Comments Template
 *
 * Closing li is left out on purpose!.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/review.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>


<div id="comment-<?php comment_ID(); ?>" class="media response-info">
    <?php
    // Если модерация комментариев ключена
    if ( '0' === $comment->comment_approved ) : ?>

        <p class="meta"><em><?php esc_attr_e( 'Your comment is awaiting approval', 'woocommerce' ); ?></em></p>

    <?php else : ?>

        <div class="media-left response-text-left">
            <a href="<?php echo get_comment_author_url( $comment );?>">

                <?php
                /**
                 * The woocommerce_review_before hook
                 *
                 * @hooked woocommerce_review_display_gravatar - 10
                 */
                do_action( 'woocommerce_review_before', $comment );

                do_action( 'woocommerce_review_meta', $comment );

                ?>

            </a>
            <h5><a href="<?php echo get_comment_author_url( $comment );?>"><?php echo get_comment_author( $comment )?></a></h5>
        </div>


    <?php endif;?>



    <div class="media-body response-text-right">

        <p>

            <?php
            do_action( 'woocommerce_review_before_comment_meta', $comment );
            do_action( 'woocommerce_review_before_comment_text', $comment );

            do_action( 'woocommerce_review_comment_text', $comment );

            do_action( 'woocommerce_review_after_comment_text', $comment ); ?>

        </p>

        <ul>
            <li><?php echo get_comment_date( 'F jS, Y' ); ?></li>
            <li><a href="#respond" onclick='return addComment.moveForm("comment-<?php comment_ID();?>", "<?php comment_ID();?>", "respond", "")' >Reply</a></li>
        </ul>
    </div>
    <div class="clearfix"> </div>
</div>